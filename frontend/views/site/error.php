<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception|null */
/* @var $authUser common\models\User */

use yii\helpers\Html;

$this->title = $exception ? ($exception->getCode() ? '#' . $exception->getCode() : 'Ошибка') : 'Ошибка';
$this->params['authUser'] = $authUser ? $authUser->username : null;
?>
<div class="site-error">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-danger">
        <?= nl2br(Html::encode($exception ? $exception->getMessage() : 'Ошибка')) ?>
    </div>

    <p>
        Вышеуказанная ошибка произошла, когда веб-сервер обрабатывал ваш запрос.
    </p>
    <p>
        Пожалуйста, свяжитесь с нами, если считаете, что это ошибка сервера. Спасибо.
    </p>

</div>
