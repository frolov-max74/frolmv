<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use frontend\assets\PortfolioAsset;

PortfolioAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body data-spy="scroll">
<?php $this->beginBody() ?>
<div id="preloader"></div>
<?= $this->render('//portfolio/_menu') ?>

<?= $content ?>

<?= $this->render('//portfolio/_footer') ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
