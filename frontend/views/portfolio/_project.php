<?php

use \yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Project */

?>
<div class="col-md-4">
    <div class="thumbnail">
        <?= Html::a(Html::img($model->image, ['class' => 'img-responsive']), '/portfolio/project/' . $model->id) ?>
        <?= Html::tag('p', Html::a($model->name, '/portfolio/project/' . $model->id), ['class' => 'project-name']) ?>
    </div>
</div>
