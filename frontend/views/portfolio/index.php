<?php

use yii\helpers\{
    Html,
    Url
};

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $contact \frontend\models\ContactForm */
/* @var $portfolio \common\models\Portfolio */
/* @var $testimonials \common\models\Testimonial */

$session = Yii::$app->session;
$this->title = $portfolio ? $portfolio->fullName . ' | Портфолио' : 'Портфолио';
?>

<section id="up" class="parallax-section" data-stellar-background-ratio="0.3">
    <div class="parallax-inner">
        <div class="parallax-content">
            <div class="container text-center">
                <h2>Привет! Я</h2>
                <div class="flexslider">
                    <ul class="slides">
                        <li>
                            <h1><?= $portfolio->fullName ?? '' ?></h1>
                        </li>
                        <li>
                            <h1>Разработчик</h1>
                        </li>
                        <li>
                            <h1>PHP Разработчик</h1>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!--end up section-->

<section id="about" class="about-section overflow-hidden">
    <div class="container">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <div class="text-center">

                    <?= $portfolio ? Html::img($portfolio->avatar, [
                        'class' => 'img-circle margin-btm-30 wow animated fadeInUp',
                        'alt' => 'avatar',
                        'data-wow-delay' => '0.2s',
                    ]) : '' ?>

                    <h1>
                        Привет! Меня зовут <span><?= $portfolio->fullName ?? '' ?>.</span> PHP Разработчик из
                        Екатеринбурга, Россия.
                    </h1>
                </div>
                <div class="separator"></div>
                <div class="row">
                    <div class="col-sm-7 wow animated fadeInLeft" data-wow-delay="0.4s">
                        <?= $portfolio->about ?? '' ?>
                    </div>
                    <div class="col-sm-5 wow animated fadeInRight" data-wow-delay="0.6s">

                        <?php if ($portfolio): ?>
                            <?php foreach ($portfolio->skills as $skill) : ?>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar"
                                         aria-valuenow="<?= $skill->value ?>"
                                         aria-valuemin="0"
                                         aria-valuemax="100"
                                         style="width: <?= $skill->value ?>%;">
                                        <?= $skill->name ?>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        <?php endif; ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--end about section-->

<section class="portfolio" id="portfolio">
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2 text-center wow animated bounceIn" data-wow-delay="0.2s">
                <h1>Некоторые мои проекты, разработанные недавно. Буду рад работать с вами.</h1>
            </div>
        </div>
        <div class="separator"></div>
    </div>
    <div class="container-fluid">
        <div class="row">

            <?php if ($portfolio): ?>
                <?php foreach ($portfolio->projects as $i => $project) : ?>
                    <?php if ($i <= floor(count($portfolio->projects) / 3) * 3 - 1): ?>
                        <div class="col-sm-4">
                            <div class="folio-box">
                                <?= Html::a(
                                    Html::img($project->image, ['class' => 'img-responsive', 'alt' => '',]),
                                    $project->image,
                                    ['data-lightbox' => 'work',]
                                ) ?>
                                <div class="folio-desc">
                                    <h4>
                                        <?php if ($project->website): ?>
                                            <?= Html::a(
                                                $project->name, '//' . $project->website, ['target' => '_blank']
                                            ) ?>
                                        <?php else: ?>
                                            <?= $project->name ?>
                                        <?php endif; ?>
                                    </h4>
                                    <p>
                                        <?= Html::a($project->brief, 'portfolio/project/' . $project->id) ?>
                                    </p>
                                </div>
                            </div>
                        </div><!--work col-->
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>

        </div>
    </div>
    <div class="container">
        <div class="separator"></div>
        <div class="row">
            <div class="projects-ref col-sm-8 col-sm-offset-2 text-center wow animated fadeInUp" data-wow-delay="0.2s">
                <h1><?= Html::a('Посмотреть все проекты', 'portfolio/all') ?></h1>
            </div>
        </div>
    </div>
</section>
<!--end portfolio section-->

<section id="services" class="services-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3 text-center">
                <h1>Я делаю следующие вещи</h1>
            </div>
        </div>
        <div class="separator"></div>
        <div class="row">
            <ul class="list-unstyled service-list">

                <?php if ($portfolio): ?>
                    <?php foreach ($portfolio->services as $service) : ?>
                        <li class="col-sm-4 clearfix service-box">
                            <i class="<?= $service->icon_code ?>"></i>
                            <div class="content">
                                <h3><?= $service->name ?></h3>
                                <p><?= $service->description ?></p>
                            </div>
                        </li>
                    <?php endforeach; ?>
                <?php endif; ?>

            </ul>
        </div>
    </div>
</section>
<!--end services section-->

<section id="testimonials" class="testimonials" data-stellar-background-ratio="0.3">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3 text-center">
                <h1>Что говорят клиенты о моей работе</h1>
            </div>
        </div>
        <div class="separator"></div>
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2 text-center">
                <div class="testi-slider">
                    <ul class="slides">

                        <?php if ($testimonials): ?>
                            <?php foreach ($testimonials as $testimonial) : ?>
                                <li>
                                    <p><?= $testimonial->project_name ?></p>
                                    <p><?= $testimonial->feedback ?></p>
                                    <?php if ($testimonial->profile->avatar): ?>
                                        <?= Html::img($testimonial->profile->avatar, [
                                            'class' => 'img-circle',
                                            'width' => '80',
                                            'alt' => '',
                                        ]) ?>
                                    <?php else: ?>
                                        <?= Html::img('/portfolio/images/noimage.png', [
                                            'class' => 'img-circle',
                                            'width' => '80',
                                            'alt' => '',
                                        ]) ?>
                                    <?php endif; ?>

                                    <h5><?= $testimonial->profile->fullName ?></h5>
                                </li>
                            <?php endforeach; ?>
                        <?php endif; ?>

                    </ul>
                </div>
            </div>
        </div>
        <div class="space50 clearfix"></div>
    </div>
</section>
<!--end testimonials section-->

<section id="contact" class="contact-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3 text-center">
                <h1>Не стесняйтесь связаться со мной</h1>
            </div>
        </div>
        <div class="separator"></div>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1 margin-btm-30">
                <p><i class="ion-iphone"></i>
                    <?= $portfolio ? Html::a(
                        $portfolio->phone,
                        Url::to('tel:' . preg_replace('/[\D]/', '', $portfolio->phone))
                    ) : '' ?>
                </p>
                <p>
                    <?= $portfolio ? Html::a(
                        $portfolio->email,
                        Url::to('mailto:' . $portfolio->email)
                    ) : '' ?>
                </p>
                <p>г. Екатеринбург, Россия, 620010</p>
                <hr>
                <ul class="list-inline wow animated fadeInUp" data-wow-delay="0.2s">
                    <li>
                        <?= $portfolio ? Html::a(
                            '<i class="ion ion-logo-whatsapp"></i>',
                            Url::to('https://api.whatsapp.com/send?phone=' .
                                preg_replace('/[\D]/', '', $portfolio->phone)),
                            ['target' => '_blank']
                        ) : '' ?>
                    </li>
                    <li>
                        <?= Html::a(
                            '<i class="ion ion-logo-skype"></i>',
                            Url::to('skype:frolov-max74')
                        ) ?>
                    </li>
                    <li>
                        <?= Html::a(
                            '<i class="ion ion-logo-vk"></i>',
                            Url::to('https://vk.com/id155953367'),
                            ['target' => '_blank']
                        ) ?>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--end contact section-->
