<?php

/* @var $this yii\web\View */

use yii\bootstrap\{
    NavBar,
    Nav
};

NavBar::begin([
    'brandLabel' => '',
    'brandUrl' => Yii::$app->homeUrl,
    'innerContainerOptions' => [
        'class' => 'container-fluid',
    ],
    'options' => [
        'class' => 'navbar-inverse navbar-fixed-top',
    ],
]);
echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-left smooth-scroll'],
    'items' => [
        ['label' => Yii::$app->name, 'url' => Yii::$app->homeUrl],
    ],
]);
echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right smooth-scroll'],
    'items' => [
        ['label' => 'Верх', 'url' => '#up'],
        ['label' => 'Обо мне', 'url' => '#about'],
        ['label' => 'Портфолио', 'url' => '#portfolio'],
        ['label' => 'Услуги', 'url' => '#services'],
        ['label' => 'Контакты', 'url' => '#contact'],
    ],
]);
NavBar::end();
