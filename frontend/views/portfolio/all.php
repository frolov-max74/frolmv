<?php

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $authUser common\models\User */

use yii\widgets\ListView;
use yii\widgets\Pjax;

$session = Yii::$app->session;
$this->title = 'Проекты';
$this->params['breadcrumbs'][] = $this->title;
$this->params['authUser'] = $authUser ? $authUser->username : null;
?>
<h1>Все проекты</h1>
<hr>
<div class="row">
    <?php Pjax::begin(); ?>    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_project',
        'layout' => "{items}\n{pager}",
    ]); ?>
    <?php Pjax::end(); ?>
</div>
