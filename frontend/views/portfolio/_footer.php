<footer class="footer">
    <div class="container text-center">
        <span>&copy; <?= Yii::$app->name  . '. 2016-' . date('Y') ?></span>
    </div>
</footer>
