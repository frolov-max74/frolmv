<?php

/* @var $this yii\web\View */
/* @var $project \common\models\Project */
/* @var $authUser common\models\User */

$session = Yii::$app->session;
$this->title = $project->name;
$this->params['breadcrumbs'][] = ['label' => 'Проекты', 'url' => ['all']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['authUser'] = $authUser ? $authUser->username : null;
?>
<div class="row">
    <div class="col-md-12">
        <h1><?php echo $project->name ?></h1>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <img class="img-project img-responsive"
             src="<?php echo $project->image ?>" alt="<?php echo $project->brief ?>">
    </div>
    <div class="col-md-6">
        <?php if ($project->brief_reference): ?>
            <p><strong>Бриф на разработку сайта: </strong><a
                        href="<?php echo $project->brief_reference ?>"
                        target="_blank"><?php echo $project->brief ?></a></p>
        <?php else: ?>
            <p><strong>Краткое описание: </strong><?php echo $project->brief ?></p>
        <?php endif; ?>

        <?php if ($project->website): ?>
            <p><strong>Адрес: </strong><a
                        href="//<?php echo $project->website ?>"
                        target="_blank"><?php echo $project->website ?></a>
            </p>
        <?php endif; ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <h2>Описание проекта</h2>
        <?php echo $project->description ?>
    </div>
</div>
