<?php

$session = Yii::$app->session;

?>
<div id="success">
    <?php if ($session->hasFlash('success')) : ?>
        <div class="alert alert-success alert-dismissable" style="text-align: center">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?= $session->getFlash('success') ?>
        </div>
    <?php endif ?>
</div>
<div id="error">
    <?php if ($session->hasFlash('error')) : ?>
        <div class="alert alert-danger alert-dismissable" style="text-align: center">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?= $session->getFlash('error') ?>
        </div>
    <?php endif ?>
</div>
