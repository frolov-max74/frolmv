<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Profile */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $authUser common\models\User */

$this->title = 'Профиль';
$this->params['breadcrumbs'][] = $this->title;
$this->params['authUser'] = $authUser->username;
?>

<div class="cabinet-profile-index">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Пожалуйста, заполните следующие поля вашего профиля:</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(); ?>

            <?= $model->avatar ? Html::img(
                Yii::$app->params['baseUrl'] . $model->avatar,
                ['class' => 'img-circle img-responsive']
            ) : ''; ?>

            <?= $form->field($model, 'imageFile')->fileInput()->label('Аватар') ?>

            <?= $form->field($model, 'first_name')->textInput(['maxlength' => true])->label('Имя') ?>

            <?= $form->field($model, 'last_name')->textInput(['maxlength' => true])->label('Фамилия') ?>

            <?= $form->field($model, 'website')->textInput(['maxlength' => true])->label('Сайт') ?>

            <div class="form-group">
                <?= Html::submitButton('Обновить', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
