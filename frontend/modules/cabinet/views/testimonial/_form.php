<?php

use yii\{
    helpers\Html,
    widgets\ActiveForm
};

/* @var $this yii\web\View */
/* @var $model common\models\Testimonial */
/* @var $form yii\widgets\ActiveForm */
/* @var $authUser common\models\User */
?>

<div class="testimonial-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'project_name')->textInput(['maxlength' => true])->label('Название проекта') ?>

    <?= $form->field($model, 'feedback')->textarea(['rows' => 6])->label('Текст отзыва') ?>

    <div class="form-group">
        <?= Html::submitButton('Оставить отзыв', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
