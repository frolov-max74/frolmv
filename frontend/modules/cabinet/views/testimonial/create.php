<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Testimonial */
/* @var $authUser common\models\User */

$this->title = 'Оставить отзыв';
$this->params['breadcrumbs'][] = $this->title;
$this->params['authUser'] = $authUser->username;
?>
<div class="testimonial-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'authUser' => $authUser,
    ]) ?>

</div>
