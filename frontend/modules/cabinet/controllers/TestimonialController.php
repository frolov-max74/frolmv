<?php

namespace frontend\modules\cabinet\controllers;

use Yii;
use yii\web\Controller;
use common\models\{
    User,
    Testimonial
};

/**
 * Testimonial controller for the `cabinet` module.
 */
class TestimonialController extends Controller
{
    /**
     * Creates a new Testimonial model.
     * @return mixed
     */
    public function actionCreate()
    {
        /* @var $authUser \common\models\User */
        $authUser = User::getAuth();
        $model = new Testimonial();

        if ($model->load(Yii::$app->request->post())) {
            $model->profile_id = $authUser->profile->id;
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Спасибо! Отзыв был успешно сохранен.');
            } else {
                Yii::$app->session->setFlash('error', 'Произошла ошибка при сохранении отзыва.');
            }
            return $this->refresh();
        } else {
            return $this->render('create', [
                'model' => $model,
                'authUser' => $authUser,
            ]);
        }
    }

}
