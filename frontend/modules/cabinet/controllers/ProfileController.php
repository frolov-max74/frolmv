<?php

namespace frontend\modules\cabinet\controllers;

use Yii;
use yii\web\Controller;
use common\models\{
    User,
    Profile
};

/**
 * Profile controller for the `cabinet` module
 */
class ProfileController extends Controller
{
    /**
     * Renders and updates an authenticated user profile view for a module.
     * @return string
     */
    public function actionIndex()
    {
        $model = Profile::findAuthUserProfile();
        if ($model->load(Yii::$app->request->post())) {
            $model->uploadAvatar();
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Профиль был успешно сохранен.');
            } else {
                Yii::$app->session->setFlash('error', 'Произошла ошибка при сохранении профиля.');
            }

            return $this->refresh();
        }
        return $this->render('index', [
            'model' => $model,
            'authUser' => User::getAuth(),
        ]);
    }
}
