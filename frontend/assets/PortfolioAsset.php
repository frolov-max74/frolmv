<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Portfolio frontend application asset bundle.
 */
class PortfolioAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'portfolio/css/animate.css',
        //'portfolio/ion-icons/css/ionicons.min.css',
        'https://unpkg.com/ionicons@4.5.10-0/dist/css/ionicons.min.css',
        'portfolio/flexslider/flexslider.css',
        'portfolio/lightbox2/dist/css/lightbox.css',
        'portfolio/css/style.css',
    ];
    public $js = [
        'portfolio/js/wow.min.js',
        'portfolio/js/jquery.stellar.min.js',
        'portfolio/flexslider/jquery.flexslider-min.js',
        'portfolio/lightbox2/dist/js/lightbox.min.js',
        'portfolio/js/custom.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'frontend\assets\IE9',
    ];
}
