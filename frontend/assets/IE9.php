<?php

namespace frontend\assets;

use yii\web\{
    AssetBundle,
    View
};

/**
 * For IE9 frontend application asset bundle.
 * @package frontend\assets
 */
class IE9 extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $js = [
        'https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js',
        'https://oss.maxcdn.com/respond/1.4.2/respond.min.js',
    ];
    public $jsOptions = [
        'position' => View::POS_HEAD,
        'condition' => 'lt IE9',
    ];
}
