<?php

namespace frontend\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\{
    Controller,
    NotFoundHttpException
};
use frontend\models\ContactForm;
use common\models\{
    Portfolio, Testimonial, Project, User
};

/**
 * Class PortfolioController
 * @package frontend\controllers
 */
class PortfolioController extends Controller
{
    /**
     * @return array
     */
    public function actions()
    {
        return [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
                'backColor' => 0x222222,
                'foreColor' => 0x209B60,
                'minLength' => 6,
                'maxLength' => 6,
                'offset' => 4,
            ],
        ];
    }

    /**
     * Displays portfolio page
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->layout = 'portfolio';

        $portfolio = Portfolio::findMyActive();
        $testimonials = Testimonial::findAllActive();
        $contact = new ContactForm();
        if ($contact->load(Yii::$app->request->post()) && $contact->validate()) {
            if ($contact->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Спасибо за обращение. Я отвечу вам в ближайшее время.');
            } else {
                Yii::$app->session->setFlash('error', 'При отправке электронной почты произошла ошибка.');
            }

            return $this->refresh('#contact');
        } else {
            return $this->render('index', [
                'contact' => $contact,
                'portfolio' => $portfolio,
                'testimonials' => $testimonials,
            ]);
        }
    }

    /**
     * Displays all projects page
     * @return string
     */
    public function actionAll()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Project::findAllActive(),
            'pagination' => [
                'pageSize' => 6,
                'pageSizeParam' => false,
            ],
        ]);

        return $this->render('all', [
            'dataProvider' => $dataProvider,
            'authUser' => User::getAuth(),
        ]);
    }

    /**
     * Displays a single Project model.
     * @param integer $id
     * @return mixed
     */
    public function actionProject($id)
    {
        $project = $this->findProject($id);
        if ($project) {
            return $this->render('view', [
                'project' => $project,
                'authUser' => User::getAuth(),
            ]);
        }
        return $this->render('site/error', [
            'authUser' => User::getAuth(),
        ]);
    }

    /**
     * Finds the Project model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Project the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findProject($id)
    {
        if (($model = Project::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемая страница не найдена.', 404);
        }
    }
}
