<?php

use yii\db\Migration;

/**
 * Handles the creation of table `skill`.
 */
class m170210_144441_create_skill_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%skill}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50)->notNull(),
            'value' => $this->smallInteger(2)->notNull(),
            'portfolio_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx-skill-portfolio_id', '{{%skill}}', 'portfolio_id');
        $this->addForeignKey('fk-skill-portfolio', '{{%skill}}', 'portfolio_id', '{{%portfolio}}', 'id', 'CASCADE', 'RESTRICT');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%skill}}');
    }
}
