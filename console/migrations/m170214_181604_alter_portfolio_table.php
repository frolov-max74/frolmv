<?php

use yii\db\Migration;

class m170214_181604_alter_portfolio_table extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%portfolio}}', 'phone', 'string(20) NOT NULL');
    }

    public function down()
    {
        $this->alterColumn('{{%portfolio}}', 'phone', 'integer(11) NOT NULL');
    }
}
