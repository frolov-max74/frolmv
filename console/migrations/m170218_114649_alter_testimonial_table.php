<?php

use yii\db\Migration;

class m170218_114649_alter_testimonial_table extends Migration
{
    public function up()
    {
        $this->dropForeignKey('fk-testimonial-user', '{{%testimonial}}');
        $this->dropIndex('idx-testimonial-user_id', '{{%testimonial}}');
        $this->dropColumn('{{%testimonial}}', 'avatar');
        $this->dropColumn('{{%testimonial}}', 'user_id');
        $this->addColumn('{{%testimonial}}', 'profile_id', 'integer NOT NULL');
        $this->createIndex('idx-testimonial-profile_id', '{{%testimonial}}', 'profile_id');
        $this->addForeignKey('fk-testimonial-profile', '{{%testimonial}}', 'profile_id', '{{%profile}}', 'id', 'CASCADE', 'RESTRICT');
    }

    public function down()
    {
        $this->dropForeignKey('fk-testimonial-profile', '{{%testimonial}}');
        $this->dropIndex('idx-testimonial-profile_id', '{{%testimonial}}');
        $this->dropColumn('{{%testimonial}}', 'profile_id');
        $this->addColumn('{{%testimonial}}', 'user_id', 'integer NOT NULL');
        $this->addColumn('{{%testimonial}}', 'avatar', 'string(100)');
        $this->createIndex('idx-testimonial-user_id', '{{%testimonial}}', 'user_id');
        $this->addForeignKey('fk-testimonial-user', '{{%testimonial}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'RESTRICT');
    }
}
