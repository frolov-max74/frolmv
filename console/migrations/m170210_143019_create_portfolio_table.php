<?php

use yii\db\Migration;

/**
 * Handles the creation of table `portfolio`.
 */
class m170210_143019_create_portfolio_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%portfolio}}', [
            'id' => $this->primaryKey(),
            'first_name' => $this->string(50)->notNull(),
            'last_name' => $this->string(50)->notNull(),
            'avatar' => $this->string(100)->notNull(),
            'email' => $this->string(50)->notNull(),
            'phone' => $this->integer(11)->notNull(),
            'about' => $this->text()->notNull(),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%portfolio}}');
    }
}
