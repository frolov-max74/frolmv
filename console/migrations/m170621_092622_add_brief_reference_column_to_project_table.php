<?php

use yii\db\Migration;

/**
 * Handles adding brief_reference to table `project`.
 */
class m170621_092622_add_brief_reference_column_to_project_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%project}}', 'brief_reference', 'string(150)');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%project}}', 'brief_reference');
    }
}
