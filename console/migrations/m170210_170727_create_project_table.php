<?php

use yii\db\Migration;

/**
 * Handles the creation of table `project`.
 */
class m170210_170727_create_project_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%project}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100)->notNull(),
            'brief' => $this->string()->notNull(),
            'description' => $this->text()->notNull(),
            'image' => $this->string(100)->notNull(),
            'website' => $this->string(50),
            'portfolio_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx-project-portfolio_id', '{{%project}}', 'portfolio_id');
        $this->addForeignKey('fk-project-portfolio', '{{%project}}', 'portfolio_id', '{{%portfolio}}', 'id', 'CASCADE', 'RESTRICT');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%project}}');
    }
}
