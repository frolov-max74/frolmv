<?php

use yii\db\Migration;

/**
 * Handles the creation of table `service`.
 */
class m170210_151638_create_service_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%service}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50)->notNull(),
            'description' => $this->text()->notNull(),
            'icon_code' => $this->string(100)->notNull(),
            'portfolio_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx-service-portfolio_id', '{{%service}}', 'portfolio_id');
        $this->addForeignKey('fk-service-portfolio', '{{%service}}', 'portfolio_id', '{{%portfolio}}', 'id', 'CASCADE', 'RESTRICT');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%service}}');
    }
}
