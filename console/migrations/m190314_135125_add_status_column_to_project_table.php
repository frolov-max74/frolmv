<?php

use yii\db\Migration;

/**
 * Handles adding status to table `project`.
 */
class m190314_135125_add_status_column_to_project_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn(
            '{{%project}}', 'status', $this->integer(1)->notNull()->defaultValue(0)
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%project}}', 'status');
    }
}
