<?php

use yii\db\Migration;

/**
 * Handles the creation of table `testimonial`.
 */
class m170209_144622_create_testimonial_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%testimonial}}', [
            'id' => $this->primaryKey(),
            'project_name' => $this->string(100)->notNull(),
            'feedback' => $this->text()->notNull(),
            'avatar' => $this->string(100),
            'status' => $this->smallInteger(1)->notNull()->defaultValue(0),
            'user_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx-testimonial-user_id', '{{%testimonial}}', 'user_id');
        $this->addForeignKey('fk-testimonial-user', '{{%testimonial}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'RESTRICT');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%testimonial}}');
    }
}
