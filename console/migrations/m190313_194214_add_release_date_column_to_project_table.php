<?php

use yii\db\Migration;

/**
 * Handles adding release_date to table `project`.
 */
class m190313_194214_add_release_date_column_to_project_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%project}}', 'release_date', $this->integer()->notNull());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%project}}', 'release_date');
    }
}
