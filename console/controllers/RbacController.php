<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
use common\rbac\rules\UserRoleRule;
use yii\helpers\Console;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        // removed old data
        $auth->removeAll();

        // added rule "UserRoleRule"
        $rule = new UserRoleRule();
        $auth->add($rule);

        // added the role of "admin" and is bound to her rule "UserRoleRule"
        $admin = $auth->createRole('admin');
        $admin->description = 'Administrator';
        $admin->ruleName = $rule->name;
        $auth->add($admin);

        // assigned roles to users
        $auth->assign($admin, 1);

        $this->stdout('Done!' . PHP_EOL, Console::FG_CYAN, Console::UNDERLINE);
    }

}
