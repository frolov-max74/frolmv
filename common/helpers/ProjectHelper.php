<?php

namespace common\helpers;

use common\models\Project;
use yii\helpers\Html;

/**
 * Class ProjectHelper
 * @package common\helpers
 */
class ProjectHelper extends Helper
{
    /**
     * Returns an array of pairs (status value => name).
     * @return array
     */
    public static function statusList(): array
    {
        return [
            Project::STATUS_INACTIVE => 'Неактивный',
            Project::STATUS_ACTIVE => 'Активный',
            Project::STATUS_SELECTED => 'Избраный',
        ];
    }

    /**
     * Returns a span tag with a value and class depending on the status passed.
     * @param $status
     * @return string
     */
    public static function statusLabel($status): string
    {
        switch ($status) {
            case Project::STATUS_INACTIVE:
                $class = 'label label-danger';
                break;
            case Project::STATUS_ACTIVE:
                $class = 'label label-success';
                break;
            case Project::STATUS_SELECTED:
                $class = 'label label-primary';
                break;
            default:
                $class = 'label label-default';
        }

        return Html::tag('span', self::statusName($status), [
            'class' => $class,
        ]);
    }
}
