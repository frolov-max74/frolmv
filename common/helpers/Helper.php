<?php

namespace common\helpers;

use yii\helpers\ArrayHelper;

/**
 * Class Helper
 * @package common\helpers
 */
abstract class Helper
{
    /**
     * Returns a list of statuses.
     * @return array
     */
    abstract public static function statusList(): array;

    /**
     * Returns the name of the status depending on the status value passed.
     * @param $status
     * @return string
     */
    protected static function statusName($status): string
    {
        return ArrayHelper::getValue(static::statusList(), $status);
    }

    /**
     * Returns status label depending on the status passed.
     * @param $status
     * @return string
     */
    abstract public static function statusLabel($status): string;
}
