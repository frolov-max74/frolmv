<?php

namespace common\helpers;

use common\models\Testimonial;
use yii\helpers\Html;

/**
 * Class TestimonialHelper
 * @package common\helpers
 */
class TestimonialHelper extends Helper
{
    /**
     * Returns an array of pairs (status value => name).
     * @return array
     */
    public static function statusList(): array
    {
        return [
            Testimonial::STATUS_INACTIVE => 'Неактивный',
            Testimonial::STATUS_ACTIVE => 'Активный',
        ];
    }

    /**
     * Returns a span tag with a value and class depending on the status passed.
     * @param $status
     * @return string
     */
    public static function statusLabel($status): string
    {
        switch ($status) {
            case Testimonial::STATUS_INACTIVE:
                $class = 'label label-danger';
                break;
            case Testimonial::STATUS_ACTIVE:
                $class = 'label label-success';
                break;
            default:
                $class = 'label label-default';
        }

        return Html::tag('span', self::statusName($status), [
            'class' => $class,
        ]);
    }
}
