<?php

namespace common\models\query;

use common\models\Portfolio;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\common\models\Portfolio]].
 *
 * @see \common\models\Portfolio
 */
class PortfolioQuery extends ActiveQuery
{
    public function active()
    {
        return $this->andWhere('[[id]]=1');
    }

    /**
     * @inheritdoc
     * @return Portfolio[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Portfolio|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
