<?php

namespace common\models\query;

use yii\db\ActiveQuery;
use common\models\Testimonial;

/**
 * This is the ActiveQuery class for [[\common\models\Testimonial]].
 *
 * @see \common\models\Testimonial
 */
class TestimonialQuery extends ActiveQuery
{
    /**
     * @return $this
     */
    public function active()
    {
        return $this->andWhere(['status' => Testimonial::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     * @return Testimonial[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Testimonial|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
