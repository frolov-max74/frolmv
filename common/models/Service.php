<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%service}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $icon_code
 * @property integer $portfolio_id
 *
 * @property Portfolio $portfolio
 */
class Service extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%service}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description', 'icon_code', 'portfolio_id'], 'required'],
            [['description'], 'string'],
            [['portfolio_id'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['icon_code'], 'string', 'max' => 100],
            [['portfolio_id'], 'exist', 'skipOnError' => true, 'targetClass' => Portfolio::className(), 'targetAttribute' => ['portfolio_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'icon_code' => 'Icon Code',
            'portfolio_id' => 'Portfolio ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPortfolio()
    {
        return $this->hasOne(Portfolio::className(), ['id' => 'portfolio_id']);
    }
}
