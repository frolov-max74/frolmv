<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\ActiveQuery;
use common\models\query\PortfolioQuery;
use yii\web\UploadedFile;
use common\components\Image;

/**
 * This is the model class for table "{{%portfolio}}".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $avatar
 * @property string $email
 * @property string $phone
 * @property string $about
 *
 * @property Project[] $projects
 * @property Service[] $services
 * @property Skill[] $skills
 * @property string $fullName
 */
class Portfolio extends ActiveRecord
{
    public $imageFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%portfolio}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'avatar', 'email', 'phone', 'about'], 'required'],
            [['about'], 'string'],
            [['email'], 'email'],
            [['first_name', 'last_name', 'email'], 'string', 'max' => 50],
            [['first_name', 'last_name', 'about', 'email'], 'trim'],
            [['imageFile'], 'file', 'maxSize' => 1024 * 1024, 'extensions' => 'png, jpg, jpeg, gif', 'checkExtensionByMimeType' => false],
            [['avatar'], 'string', 'max' => 100],
            [['phone'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'imageFile' => 'Avatar',
            'email' => 'Email',
            'phone' => 'Phone',
            'about' => 'About',
        ];
    }

    /**
     * @return array|Portfolio|null
     */
    public static function findMyActive()
    {
        return self::find()->active()->with('projects', 'services', 'skills')->one();
    }

    /**
     * @return int
     */
    public static function getActiveId()
    {
        return self::find()->active()->one()->id;
    }

    /**
     * @inheritdoc
     * @return PortfolioQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PortfolioQuery(get_called_class());
    }

    /**
     * @return string full name
     */
    public function getFullName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * Upload and resizing avatar
     */
    public function uploadAvatar()
    {
        $basePath = Yii::getAlias('@frontend/web/');

        if ($this->imageFile = UploadedFile::getInstance($this, 'imageFile')) {
            $srcPath = $basePath . 'upload/images/portfolio/' . $this->imageFile->baseName . '.' . $this->imageFile->extension;
            if ($this->imageFile->saveAs($srcPath)) {
                $this->deleteAvatarFile();
                $image = new Image();
                $thumbPath = 'upload/images/portfolio/avatar-' . time() . '.' . $this->imageFile->extension;
                $destPath = $basePath . $thumbPath;
                if ($image->resizeImage($srcPath, $destPath, 128, 128, 80)) {
                    $this->avatar = '/' . $thumbPath;
                    unlink($srcPath);
                }
            }
        }
    }

    /**
     * Before deleting a record deletes the avatar file
     * @return bool
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $this->deleteAvatarFile();
            return true;
        } else {
            return false;
        }
    }

    /**
     * Deletes the avatar file
     */
    protected function deleteAvatarFile()
    {
        if (is_file(Yii::getAlias('@frontend/web' . $this->avatar))) {
            unlink(Yii::getAlias('@frontend/web' . $this->avatar));
        }
    }

    /**
     * @return array all ids
     */
    public static function findAllIds()
    {
        return self::find()
            ->select('id')
            ->orderBy('id')
            ->indexBy('id')
            ->column();
    }

    /**
     * @return ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasMany(Project::class, ['portfolio_id' => 'id'])
            ->andFilterWhere(['status' => Project::STATUS_SELECTED])
            ->orderBy(['release_date' => SORT_DESC])
            ->limit(9);
    }

    /**
     * @return ActiveQuery
     */
    public function getServices()
    {
        return $this->hasMany(Service::class, ['portfolio_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getSkills()
    {
        return $this->hasMany(Skill::class, ['portfolio_id' => 'id']);
    }
}
