<?php

namespace common\models;

use Yii;
use yii\web\UploadedFile;
use common\components\Image;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%project}}".
 *
 * @property integer $id
 * @property string $name
 * @property integer $release_date
 * @property string $brief
 * @property string $description
 * @property string $image
 * @property string $website
 * @property integer $portfolio_id
 * @property integer $status
 * @property string $brief_reference
 *
 * @property Portfolio $portfolio
 */
class Project extends ActiveRecord
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_SELECTED = 2;

    public $imageFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%project}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'brief', 'description', 'image', 'portfolio_id', 'release_date', 'status'], 'required'],
            [['description'], 'string'],
            [['portfolio_id', 'status'], 'integer'],
            ['status', 'default', 'value' => self::STATUS_INACTIVE],
            ['status', 'in', 'range' => [self::STATUS_INACTIVE, self::STATUS_ACTIVE, self::STATUS_SELECTED]],
            [['name', 'image'], 'string', 'max' => 100],
            [['release_date'], 'date', 'format' => 'dd-MM-yyyy', 'timestampAttribute' => 'release_date'],
            [['brief'], 'string', 'max' => 255],
            [['website'], 'string', 'max' => 50],
            [['brief_reference'], 'string', 'max' => 150],
            [['name', 'brief', 'description', 'website', 'brief_reference'], 'trim'],
            [['imageFile'], 'file', 'maxSize' => 1024 * 1024, 'extensions' => 'png, jpg, jpeg', 'checkExtensionByMimeType' => false],
            [['portfolio_id'], 'exist', 'skipOnError' => true, 'targetClass' => Portfolio::class, 'targetAttribute' => ['portfolio_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'release_date' => 'Release Date',
            'brief' => 'Brief',
            'description' => 'Description',
            'imageFile' => 'Image',
            'website' => 'Website',
            'portfolio_id' => 'Portfolio ID',
            'brief_reference' => 'Brief Reference',
            'status' => 'Status',
        ];
    }

    /**
     * Upload and resizing project image
     */
    public function uploadImage()
    {
        $basePath = Yii::getAlias('@frontend/web/');

        if ($this->imageFile = UploadedFile::getInstance($this, 'imageFile')) {
            $srcPath = $basePath . 'upload/images/portfolio/' . $this->imageFile->baseName . '.' . $this->imageFile->extension;
            if ($this->imageFile->saveAs($srcPath)) {
                $this->deleteImageFile();
                $image = new Image();
                $thumbPath = 'upload/images/portfolio/project-' . $this->imageFile->baseName . '.' . $this->imageFile->extension;
                $destPath = $basePath . $thumbPath;
                if ($image->resizeImage($srcPath, $destPath, 800, 600, 80)) {
                    $this->image = '/' . $thumbPath;
                    unlink($srcPath);
                }
            }
        }
    }

    /**
     * Before deleting a record deletes the image file
     * @return bool
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $this->deleteImageFile();
            return true;
        } else {
            return false;
        }
    }

    /**
     * Deletes the image file
     */
    protected function deleteImageFile()
    {
        if (is_file(Yii::getAlias('@frontend/web' . $this->image))) {
            unlink(Yii::getAlias('@frontend/web' . $this->image));
        }
    }

    /**
     * @return yii\Db\ActiveQuery
     */
    public static function findAllActive()
    {
        return self::find()
            ->select('id, name, image')
            ->where(['portfolio_id' => Portfolio::getActiveId(), 'status' => [self::STATUS_ACTIVE, self::STATUS_SELECTED]])
            ->orderBy(['release_date' => SORT_DESC]);
    }

    /**
     * @return array
     */
    public static function findAllActiveByArray()
    {
        return Yii::$app->db
            ->createCommand(
                'SELECT id, name, image ' .
                'FROM project ' .
                'WHERE portfolio_id=:portfolio_id AND status=:status ' .
                'ORDER BY release_date DESC'
            )
            ->bindValues([':portfolio_id' => Portfolio::getActiveId(), ':status' => self::STATUS_ACTIVE])
            ->queryAll();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPortfolio()
    {
        return $this->hasOne(Portfolio::class, ['id' => 'portfolio_id']);
    }
}
