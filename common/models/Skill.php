<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%skill}}".
 *
 * @property integer $id
 * @property string $name
 * @property integer $value
 * @property integer $portfolio_id
 *
 * @property Portfolio $portfolio
 */
class Skill extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%skill}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'value', 'portfolio_id'], 'required'],
            [['value', 'portfolio_id'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['portfolio_id'], 'exist', 'skipOnError' => true, 'targetClass' => Portfolio::className(), 'targetAttribute' => ['portfolio_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'value' => 'Value',
            'portfolio_id' => 'Portfolio ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPortfolio()
    {
        return $this->hasOne(Portfolio::className(), ['id' => 'portfolio_id']);
    }
}
