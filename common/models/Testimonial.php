<?php

namespace common\models;

use yii\db\ActiveRecord;
use yii\db\ActiveQuery;
use common\models\query\TestimonialQuery;

/**
 * This is the model class for table "{{%testimonial}}".
 *
 * @property integer $id
 * @property string $project_name
 * @property string $feedback
 * @property integer $status
 * @property integer $profile_id
 *
 * @property Profile $profile
 */
class Testimonial extends ActiveRecord
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%testimonial}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_name', 'feedback', 'profile_id'], 'required', 'message' => 'Пожалуйста, заполните поле.'],
            [['feedback'], 'string'],
            [['status', 'profile_id'], 'integer'],
            ['status', 'default', 'value' => self::STATUS_INACTIVE],
            ['status', 'in', 'range' => [self::STATUS_INACTIVE, self::STATUS_ACTIVE]],
            [['project_name'], 'string', 'max' => 100],
            [['profile_id'], 'exist', 'skipOnError' => true, 'targetClass' => Profile::className(), 'targetAttribute' => ['profile_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project_name' => 'Project Name',
            'feedback' => 'Feedback',
            'status' => 'Status',
            'profile_id' => 'Profile ID',
        ];
    }

    /**
     * @return array|Testimonial[]
     */
    public static function findAllActive()
    {
        return self::find()->active()->with('profile')->all();
    }

    /**
     * @return ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['id' => 'profile_id']);
    }

    /**
     * @inheritdoc
     * @return TestimonialQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TestimonialQuery(get_called_class());
    }
}
