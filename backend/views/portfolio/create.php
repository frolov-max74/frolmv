<?php

/* @var $this yii\web\View */
/* @var $model common\models\Portfolio */

$this->title = 'Create Portfolio';
$this->params['breadcrumbs'][] = ['label' => 'Portfolio', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Create';
?>
<div class="portfolio-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
