<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Portfolio';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="portfolio-index">

    <p>
        <?= Html::a('Create Portfolio', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'first_name',
            'last_name',
            'email:email',
            'phone',
            // 'about:ntext',
            [
                'attribute' => 'avatar',
                'label' =>false,
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->avatar ? Html::img(
                        Yii::$app->params['baseUrl'] . $data->avatar,
                        [
                            'class' => 'img-circle img-responsive',
                            'style' => 'width: 50px',
                        ]
                    ) : '';
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
