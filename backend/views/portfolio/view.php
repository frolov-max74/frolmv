<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Portfolio */

$this->title = 'Portfolio: ' . $model->fullName;
$this->params['breadcrumbs'][] = ['label' => 'Portfolio', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->fullName;
?>
<div class="portfolio-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id',
            [
                'attribute' => 'avatar',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->avatar ? Html::img(
                        Yii::$app->params['baseUrl'] . $model->avatar,
                        [
                            'class' => 'img-circle img-responsive',
                            'style' => 'width:100px',
                        ]
                    ) : '';
                }
            ],
            'first_name',
            'last_name',
            'email:email',
            'phone',
            'about:ntext',
        ],
    ]) ?>

</div>
