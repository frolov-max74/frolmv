<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form
        ->field($model, 'status')
        ->dropDownList(
            [$model::STATUS_ACTIVE => 'Да', $model::STATUS_INACTIVE => 'Нет'],
            ['prompt' => 'Выбрать',]
        )
        ->label('Active') ?>

    <div class="form-group">
        <?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
