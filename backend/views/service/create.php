<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Service */
/* @var $portfolioIdsList array all Portfolio ID */

$this->title = 'Create Service';
$this->params['breadcrumbs'][] = ['label' => 'Services', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Create';
?>
<div class="service-create">

    <?= $this->render('_form', [
        'model' => $model,
        'portfolioIdsList' => $portfolioIdsList,
    ]) ?>

</div>
