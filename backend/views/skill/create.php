<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Skill */
/* @var $portfolioIdsList array all Portfolio ID */

$this->title = 'Create Skill';
$this->params['breadcrumbs'][] = ['label' => 'Skills', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Create';
?>
<div class="skill-create">

    <?= $this->render('_form', [
        'model' => $model,
        'portfolioIdsList' => $portfolioIdsList,
    ]) ?>

</div>
