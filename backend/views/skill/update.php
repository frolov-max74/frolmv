<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Skill */
/* @var $portfolioIdsList array all Portfolio ID */

$this->title = 'Update Skill: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Skills', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="skill-update">

    <?= $this->render('_form', [
        'model' => $model,
        'portfolioIdsList' => $portfolioIdsList,
    ]) ?>

</div>
