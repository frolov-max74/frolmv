<?php

use yii\helpers\Html;
use common\helpers\ProjectHelper;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use mihaildev\{
    ckeditor\CKEditor,
    elfinder\ElFinder
};

/* @var $this yii\web\View */
/* @var $model common\models\Project */
/* @var $form yii\widgets\ActiveForm */
/* @var $portfolioIdsList array all Portfolio ID */
?>

<div class="project-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form
        ->field($model, 'release_date')
        ->widget(
            DatePicker::class,
            [
                'dateFormat' => 'dd-MM-yyyy',
            ]
        ) ?>

    <?= $model->image ? Html::img(
        Yii::$app->params['baseUrl'] . $model->image,
        ['class' => 'img-responsive', 'style' => 'width: 300px']
    ) : ''; ?>

    <?= $form->field($model, 'imageFile')->fileInput() ?>

    <?= $form->field($model, 'brief')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'brief_reference')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'website')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->widget(CKEditor::class, [
        'editorOptions' => ElFinder::ckeditorOptions('elfinder', [])
    ]); ?>

    <?= $form
        ->field($model, 'status')
        ->dropDownList(
            ProjectHelper::statusList(),
            ['prompt' => 'Выбрать']
        ) ?>

    <?= $form
        ->field($model, 'portfolio_id')
        ->dropDownList(
            $portfolioIdsList,
            [
                'prompt' => 'Выбрать',
            ]
        ) ?>

    <div class="form-group">
        <?= Html::submitButton(
            $model->isNewRecord ? 'Create' : 'Update',
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
        ) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
