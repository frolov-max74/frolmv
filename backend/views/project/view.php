<?php

use yii\helpers\{
    Html,
    Url
};
use common\helpers\ProjectHelper;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Project */

$this->title = 'Project: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->name;
?>
<div class="project-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            [
                'attribute' => 'release_date',
                'format' => ['date', 'dd-MM-yyyy'],
            ],
            [
                'attribute' => 'image',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->image ? Html::img(
                        Yii::$app->params['baseUrl'] . $data->image,
                        [
                            'class' => 'img-responsive',
                            'style' => 'width: 500px',
                        ]
                    ) : '';
                }
            ],
            'brief',
            [
                'attribute' => 'brief_reference',
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a(
                        Html::encode($data->brief_reference),
                        Url::to($data->brief_reference),
                        ['target' => '_blank']
                    );
                }
            ],
            [
                'attribute' => 'website',
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a(
                        Html::encode($data->website),
                        Url::to('//' . $data->website),
                        ['target' => '_blank']
                    );
                }
            ],
            'description:ntext',
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return ProjectHelper::statusLabel($model->status);
                },
                'format' => 'raw',
            ],
            'portfolio_id',
        ],
    ]) ?>

</div>
