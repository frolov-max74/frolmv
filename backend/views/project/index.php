<?php

use yii\helpers\{
    Html,
    Url
};
use common\{
    helpers\ProjectHelper,
    models\Project
};
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\ProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $portfolioIdsList array all Portfolio ID */

$this->title = 'Projects';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-index">

    <p>
        <?= Html::a('Create Project', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="table-responsive">
        <?php Pjax::begin(); ?>    <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                // 'id',
                'name',
                [
                    'attribute' => 'release_date',
                    'format' => ['date', 'dd-MM-Y'],
                    'filter' => DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'release_date',
                        'dateFormat' => 'dd-MM-yyyy',
                    ]),
                ],
                // 'brief',
                // 'description:ntext',
                [
                    'attribute' => 'website',
                    'format' => 'raw',
                    'value' => function ($data) {
                        return Html::a(
                            Html::encode($data->website),
                            Url::to('//' . $data->website),
                            ['target' => '_blank']
                        );
                    }
                ],
                [
                    'attribute' => 'status',
                    'format' => 'raw',
                    'filter' => Html::activeDropDownList(
                        $searchModel,
                        'status',
                        ProjectHelper::statusList(),
                        ['prompt' => 'Выбрать']
                    ),
                    'value' => function (Project $searchModel) {
                        return ProjectHelper::statusLabel($searchModel->status);
                    },
                ],
                [
                    'attribute' => 'portfolio_id',
                    'filter' => Html::activeDropDownList(
                        $searchModel,
                        'portfolio_id',
                        $portfolioIdsList,
                        ['prompt' => 'Выбрать']
                    ),
                    'value' => function ($data) {
                        return $data->portfolio->id;
                    },
                ],
                [
                    'attribute' => 'image',
                    'label' => false,
                    'format' => 'raw',
                    'value' => function ($data) {
                        return $data->image ? Html::img(
                            Yii::$app->params['baseUrl'] . $data->image,
                            [
                                'class' => 'img-responsive',
                                'style' => 'width: 100px',
                            ]
                        ) : '';
                    }
                ],

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>
