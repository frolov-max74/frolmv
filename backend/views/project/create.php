<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Project */
/* @var $portfolioIdsList array all Portfolio ID */

$this->title = 'Create Project';
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Create';
?>
<div class="project-create">

    <?= $this->render('_form', [
        'model' => $model,
        'portfolioIdsList' => $portfolioIdsList,
    ]) ?>

</div>
