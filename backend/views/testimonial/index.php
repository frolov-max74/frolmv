<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\{
    helpers\TestimonialHelper,
    models\Testimonial
};

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\TestimonialSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $allFullNames array all full names */

$this->title = 'Testimonials';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="testimonial-index">

    <p>
        <?= Html::a('Create Testimonial', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="table-responsive">
        <?php Pjax::begin(); ?>    <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'project_name',
                // 'feedback:ntext',
                [
                    'attribute' => 'status',
                    'format' => 'raw',
                    'filter' => Html::activeDropDownList(
                        $searchModel,
                        'status',
                        TestimonialHelper::statusList(),
                        ['prompt' => 'Выбрать']
                    ),
                    'value' => function (Testimonial $searchModel) {
                        return TestimonialHelper::statusLabel($searchModel->status);
                    },
                ],
                [
                    'attribute' => 'profile_id',
                    'label' => 'User',
                    'filter' => Html::activeDropDownList(
                        $searchModel,
                        'profile_id',
                        $allFullNames,
                        ['prompt' => 'Выбрать']
                    ),
                    'value' => function ($data) {
                        return $data->profile->fullName;
                    }
                ],

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>
