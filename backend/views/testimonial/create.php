<?php

/* @var $this yii\web\View */
/* @var $model common\models\Testimonial */
/* @var $allFullNames array all full names */

$this->title = 'Create Testimonial';
$this->params['breadcrumbs'][] = ['label' => 'Testimonials', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Create';
?>
<div class="testimonial-create">

    <?= $this->render('_form', [
        'model' => $model,
        'allFullNames' => $allFullNames,
    ]) ?>

</div>
