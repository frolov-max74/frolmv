<?php

/* @var $this yii\web\View */
/* @var $model common\models\Testimonial */
/* @var $allFullNames array all full names */

$this->title = 'Update Testimonial: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Testimonials', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="testimonial-update">

    <?= $this->render('_form', [
        'model' => $model,
        'allFullNames' => $allFullNames,
    ]) ?>

</div>
