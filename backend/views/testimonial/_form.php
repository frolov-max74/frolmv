<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\helpers\TestimonialHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Testimonial */
/* @var $form yii\widgets\ActiveForm */
/* @var $allFullNames array all full names */
?>

<div class="testimonial-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'project_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'feedback')->textarea(['rows' => 6]) ?>

    <?= $form
        ->field($model, 'status')
        ->dropDownList(
            TestimonialHelper::statusList(),
            ['prompt' => 'Выбрать']
        ) ?>

    <?= $form
        ->field($model, 'profile_id')
        ->dropDownList($allFullNames, ['prompt' => 'Выбрать'])
        ->label('User') ?>

    <div class="form-group">
        <?= Html::submitButton(
            $model->isNewRecord ? 'Create' : 'Update',
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
        ) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
