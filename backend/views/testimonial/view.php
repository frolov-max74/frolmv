<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\{
    models\Testimonial,
    helpers\TestimonialHelper
};

/* @var $this yii\web\View */
/* @var $model common\models\Testimonial */

$this->title = 'Testimonial: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Testimonials', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->id;
?>
<div class="testimonial-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'project_name',
            'feedback:ntext',
            [
                'attribute' => 'status',
                'value' => function (Testimonial $model) {
                    return TestimonialHelper::statusLabel($model->status);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'profile_id',
                'label' => 'User',
                'value' => function ($data) {
                    return $data->profile->fullName;
                },
            ],
            'profile_id',
        ],
    ]) ?>

</div>
